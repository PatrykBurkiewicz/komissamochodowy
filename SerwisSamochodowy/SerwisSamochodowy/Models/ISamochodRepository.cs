﻿using System.Collections.Generic;

namespace SerwisSamochodowy.Models
{
    public interface ISamochodRepository
    {
        IEnumerable<Samochod> PobierzWszystkieSamochody();
        Samochod PobierzSamochodOId(int samochodId);
    }
}
