﻿using Microsoft.AspNetCore.Mvc;
using SerwisSamochodowy.Models;
using SerwisSamochodowy.ViewModels;
using System.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SerwisSamochodowy.Controllers
{
    public class HomeController : Controller
    {

        private readonly ISamochodRepository _samochodReporistory;

        public HomeController(ISamochodRepository samochodRepository)
        {
            _samochodReporistory = samochodRepository;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            //ViewBag.Tytul = "Przegląd Samochodów";

            var samochody = _samochodReporistory.PobierzWszystkieSamochody().OrderBy(s => s.Marka);

            var homeVM = new HomeVM()
            {
                Tytul = "Przeglad Samochodow",
                Samochody = samochody.ToList()
            };

            return View(homeVM);
        }
    }
}
