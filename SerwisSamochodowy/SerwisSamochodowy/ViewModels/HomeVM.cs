﻿using SerwisSamochodowy.Models;
using System.Collections.Generic;

namespace SerwisSamochodowy.ViewModels
{
    public class HomeVM
    {
        public string Tytul { get; set; }
        public List<Samochod> Samochody { get; set; }
    }
}
